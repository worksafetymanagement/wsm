IF EXISTS
(SELECT NAME FROM SYS.VIEWS WHERE NAME = 'dbo.vwAlerts')
DROP VIEW dbo.vwAlerts) go

CREATE VIEW dbo.vwAlerts AS
SELECT 
    VCV.xxxx, 
    VCV.yyyy AS yyyy
    ,VCV.zzzz AS zzzz
FROM TABLE_A