// {
// DeviceID: "",
// Temperature: "",
// Pressure: "",
// Humidity: "".
// Location: "",
// HeartRate: "", // Future
// GasConcentration: "" 
// }

using System;

namespace wsm_web.Models
{
    public class WorkerStatus
    {
        public int ID { get; set; } //PK
        public DateTime TimeStamp { get; set; }
        public int TeamID { get; set; }
        public int DeviceID { get; set; }
        public float Temperature { get; set; }
        public float Pressure { get; set; }
        public float Humidity { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public int HeartRate { get; set; }
        public float GasConcentration { get; set; }
    }
}