using System;  
using System.Collections.Generic;  
using System.ComponentModel.DataAnnotations;  
using System.Linq;  
using System.Threading.Tasks;    
using System.Data;  
using System.Data.SqlClient;  
using System.IO;
using Microsoft.Extensions.Configuration;

namespace wsm_web.Models  
{  
    public class User 
    {  
        public int ID { get; set; }

        [Required]  
        [Display(Name = "First Name")]  
        public string FirstName { get; set; }  
  
        [Required]  
        [Display(Name = "Last Name")]  
        public string LastName { get; set; }  
  
        [Required]  
        [Display(Name = "User ID")]  
        public string UserID { get; set; }  
  
        [Required]  
        [Display(Name = "Password")]  
        [DataType(DataType.Password)]  
        public string Password { get; set; }  
    }  

    public class UserDataAccessLayer  
    {  
        string connectionString = GetConnectionString();  
        public static IConfiguration Configuration { get; set; }  
        private static string GetConnectionString()  
        {  
            var builder = new ConfigurationBuilder()  
                .SetBasePath(Directory.GetCurrentDirectory())  
                .AddJsonFile("appsettings.json");  
  
            Configuration = builder.Build();  
  
            string connectionString = Configuration["ConnectionStrings:myConString"];  
  
            return connectionString;  
  
        }  
        public string RegisterUser(User user)  
        {  
            using (SqlConnection con = new SqlConnection(connectionString))  
            {  
                SqlCommand cmd = new SqlCommand("spRegisterUser", con);  
                cmd.CommandType = CommandType.StoredProcedure;  
  
                cmd.Parameters.AddWithValue("@FirstName", user.FirstName);  
                cmd.Parameters.AddWithValue("@LastName", user.LastName);  
                cmd.Parameters.AddWithValue("@UserID", user.UserID);  
                cmd.Parameters.AddWithValue("@UserPassword", user.Password);  
  
                con.Open();  
                string result = cmd.ExecuteScalar().ToString();  
                con.Close();  
  
                return result;  
            }  
        }  
        public string ValidateLogin(User user)  
        {  
            using (SqlConnection con = new SqlConnection(connectionString))  
            {  
                SqlCommand cmd = new SqlCommand("spValidateUserLogin", con);  
                cmd.CommandType = CommandType.StoredProcedure;  
  
                cmd.Parameters.AddWithValue("@LoginID", user.UserID);  
                cmd.Parameters.AddWithValue("@LoginPassword", user.Password);  
  
                con.Open();  
                string result = cmd.ExecuteScalar().ToString();  
                con.Close();  
  
                return result;  
            }  
        }  
    }  
} 