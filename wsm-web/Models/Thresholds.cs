using System;  
using System.Collections.Generic;  
using System.ComponentModel.DataAnnotations;  
using System.Linq;  
using System.Threading.Tasks;    
using System.Data;  
using System.Data.SqlClient;  
using System.IO;
using Microsoft.Extensions.Configuration;

namespace wsm_web.Models  
{
    public class Threshold
    {
        public int ID { get; set; }
        public string type { get; set; }
        public int value { get; set; }
        public int level { get; set; }
    }
} 