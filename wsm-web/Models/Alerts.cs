using System;  
using System.Collections.Generic;  
using System.ComponentModel.DataAnnotations;  
using System.Linq;  
using System.Threading.Tasks;    
using System.Data;  
using System.Data.SqlClient;  
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace wsm_web.Models  
{
    public class Alert
    {
        public int deviceID { get; set; }
        public string type { get; set; }
        public int threshold { get; set; }
        public int level { get; set; }
        public int value { get; set; }

        public Alert() { }
        private Alert(int _deviceID, string _type, int _threshold, int _level, int _value)
        {
            deviceID = _deviceID;
            type = _type;
            threshold = _threshold;
            level = _level;
            value = _value;
        }

        public List<Alert> GetAlertList(dbContext db, int teamid)
        {
            var alertList = new List<Alert>();
            var workerStatuses = db.WorkerStatus.Where(b => b.TeamID == teamid).ToList();
            var thresholds = db.Thresholds.ToList();

            foreach (WorkerStatus ws in workerStatuses) //Loop through all worker status entries
            {
                foreach (Threshold t in thresholds) //For each worker status, loop through all thresholds
                {
                    //For the current threshold t, get the matching worker status property dynamically
                    PropertyInfo wsProp = ws.GetType().GetProperty(t.type, BindingFlags.Public | BindingFlags.Instance);
                    if (Convert.ToInt32(wsProp.GetValue(ws)) > t.value) //Check if the property's value is greater than the threshold
                    {
                        //Before adding the new alert, loop through the alert list and remove all lower-level alerts 
                        //for the same alert type and device id
                        foreach (Alert a in alertList)
                        {
                            var removeAlertList = new List<Alert>();
                            if (a.type == wsProp.Name && a.deviceID == ws.DeviceID && a.level < t.level)
                            {
                                removeAlertList.Add(a);
                            }

                            if (removeAlertList.Count > 0)
                            {
                                alertList = alertList.Except(removeAlertList).ToList();
                            }
                        }

                        //Add the new alert to the list
                        alertList.Add(new Alert(ws.DeviceID, t.type, t.value, t.level, Convert.ToInt32(wsProp.GetValue(ws))));
                    }
                }
            }

            return alertList;           
        }
    }
} 