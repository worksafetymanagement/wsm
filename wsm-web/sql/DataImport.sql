SELECT TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES



SELECT * FROM Teams;
SELECT * FROM Users;
SELECT * FROM Devices;
SELECT * FROM WorkerStatus;
SELECT * FROM Thresholds;



-- DROP TABLE WorkerStatus;
-- DROP TABLE Devices;
-- DROP TABLE Users;
-- DROP TABLE Teams;
-- DROP TABLE Thresholds;
-- DROP TABLE __EFMigrationsHistory;


-- INSERT INTO table_name (column1, column2, column3, ...)
-- VALUES (value1, value2, value3, ...);

-- DELETE from Devices;
-- DELETE from Users;
-- DELETE from Teams;


SELECT top(20) * FROM dbo.WorkerStatus order by [Timestamp];


SELECT * from Teams

-- UPDATE Teams SET SupervisorId = 16 WHERE [SupervisorId] = 1

INSERT INTO Teams (Name, SupervisorId) VALUES ('Team 1', 1);

INSERT INTO Users (FirstName, LastName, Phone, TeamId, TempId) VALUES ('Amandeep', 'Singh', '212-123-1234', 4, 4);
INSERT INTO Users (FirstName, LastName, Phone, TeamId, TempId) VALUES ('Mikhail', 'Elterman', '212-333-3333', 4, 5);
INSERT INTO Users (FirstName, LastName, Phone, TeamId, TempId) VALUES ('Garett', 'Chang', '212-444-4444', 4, 6);


SELECT * from Users
INSERT INTO Users (FirstName, LastName, Phone, TeamId, TempId) VALUES ('Supervisor', '', '212-999-9999', 4, 10);


INSERT INTO Devices (DeviceId, Name, CurrentUserId, UserId, Type) VALUES (1, 'Device 1', '9', 9, 'Sensor Device');
INSERT INTO Devices (DeviceId, Name, CurrentUserId, UserId, Type) VALUES (2, 'Device 2', '12', 12, 'Sensor Device');
INSERT INTO Devices (DeviceId, Name, CurrentUserId, UserId, Type) VALUES (3, 'Device 3', '13', 13, 'Sensor Device');




    -- public enum ThresholdType : int 
    -- {
    --     Temperature = 1,
    --     Humidity,
    --     Pressure,
    --     HeartRate,
    --     GasConcentration        
    -- }

    -- public enum ThresholdCompareOperator : int 
    -- {
    --     Equals = 1,
    --     GreaterThan,
    --     LessThan
    -- }

    -- public enum AlertLevel : int 
    -- {
    --     Low = 1,
    --     Medium,
    --     High
    -- }



-- Temperature
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (1, 90, 1, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (1, 100, 2, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (1, 110, 3, 2);

INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (1, 20, 1, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (1, 10, 2, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (1, 0, 3, 3);


-- Humidity
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (2, 85, 1, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (2, 90, 2, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (2, 95, 3, 2);

INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (2, 20, 1, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (2, 15, 2, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (2, 10, 3, 3);


-- Pressure
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (3, 1100, 1, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (3, 1200, 2, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (3, 1300, 3, 2);

INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (3, 400, 1, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (3, 300, 2, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (3, 200, 3, 3);


-- HeartRate
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (4, 100, 1, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (4, 110, 2, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (4, 120, 3, 2);

INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (4, 60, 1, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (4, 55, 2, 3);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (4, 50, 3, 3);


-- GasConcentration
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (5, 3, 1, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (5, 4, 2, 2);
INSERT INTO Thresholds ([Type], [Value], AlertLevel, CompareOperator) VALUES (5, 5, 3, 2);



