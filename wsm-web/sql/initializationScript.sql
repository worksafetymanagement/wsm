-- -- Drop the dbo.WorkerStatus table if it exists 
-- if exists(select object_id from sys.objects where type='U' and name = 'WorkerStatus')
--   drop table dbo.WorkerStatus;
-- go

-- ---- Create the dbo.WorkerStatus TABLE

-- create table dbo.WorkerStatus
-- (
--     ID int IDENTITY(1,1) not null
--         constraint PK_Measurement_MeasurementID 
--       primary key CLUSTERED,
--     deviceID nvarchar(50) not null
--         constraint DF_Measurement_deviceID
--       default '',
--     [timestamp] datetime null,
--     temperature float(53),
--     pressure float(53),
--     humidity float(53),
--     heartRate int,
--     latitude float,
--     longitude float,
--     gasConcentration float(53),
-- );
-- go


SELECT top(20) * FROM dbo.WorkerStatus order by [Timestamp] desc; 


-- INSERT INTO table_name (column1, column2, column3, ...)
-- VALUES (value1, value2, value3, ...);

-- INSERT into dbo.WorkerStatus 
-- (deviceID, temperature, pressure, humidity, heartRate, latitude, longitude, gasConcentration)
-- VALUES ('AZ-Test', '20', '33', '44', 65, 40.12, 70.21, 10 );

