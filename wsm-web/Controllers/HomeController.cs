﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using wsm_web.Models;

namespace wsm_web.Controllers
{
    public class HomeController : Controller
    {
        private readonly dbContext db;
        public HomeController(dbContext db)
        {
            this.db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        //[Authorize]
        public IActionResult Dashboard()
        {
            return View();
        }
        
        public IActionResult Privacy()
        {
            return View();
        }

        public JsonResult GetAlertData(int teamid = 1)
        {
            var alert = new Alert();
            return new JsonResult(JsonConvert.SerializeObject(alert.GetAlertList(db, teamid)));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
