using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Security.Claims;  
using System.Threading.Tasks;  
using wsm_web.Models;  
using Microsoft.AspNetCore.Authentication;  
using Microsoft.AspNetCore.Mvc;  
  
namespace wsm_web.Controllers  
{  
    public class AccountController : Controller  
    {  
        UserDataAccessLayer objUser = new UserDataAccessLayer();  
  
        [HttpGet]  
        public IActionResult Register()  
        {  
            return View();  
        }  
  
        [HttpPost]  
        public IActionResult Register([Bind] User user)  
        {  
            if (ModelState.IsValid)  
            {  
                string RegistrationStatus = objUser.RegisterUser(user);  
                if (RegistrationStatus == "Success")  
                {  
                    ModelState.Clear();  
                    TempData["Success"] = "Registration Successful!";  
                    return View();  
                }  
                else  
                {  
                    TempData["Fail"] = "This User ID already exists.";  
                    return View();  
                }  
            }  
            return View();  
        }  
  
        [HttpGet]  
        public IActionResult Login()  
        {  
            return View();  
        }  
  
        [HttpPost]  
        [ValidateAntiForgeryToken]  
        public async Task<IActionResult> Login([Bind] User user)  
        {  
            ModelState.Remove("FirstName");  
            ModelState.Remove("LastName");  
  
            if (ModelState.IsValid)  
            {  
                string LoginStatus = objUser.ValidateLogin(user);  
  
                if (LoginStatus == "Success")  
                {  
                    var claims = new List<Claim>  
                    {  
                        new Claim(ClaimTypes.Name, user.UserID)  
                    };  
                    ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "login");  
                    ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);  
  
                    await HttpContext.SignInAsync(principal);  
                    return RedirectToAction("Home", "Dashboard");  
                }  
                else  
                {  
                    TempData["LoginFailed"] = "Invalid credentials.";  
                    return View();  
                }  
            }  
            else  {
                    return View();  
            }  
        }  
    }  
} 