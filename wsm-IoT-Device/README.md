# DevKitState

Monitor DevKit states and control the user LED with Azure IoT Hub device twins.


Please follow the [guide](https://github.com/IoTDevEnvExamples/DevKitState/blob/master/Device/Readme.md) to run DevKitState in IoT Workbench.