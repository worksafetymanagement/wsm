// Test the Show warningMessage by: GET -  https://wsm-function.azurewebsites.net/api/wsm-c2d?action=set&state=1&key=warningMessage&t=1532890594078

/*
{
deviceID: "Devic32",
timestamp: null,
temperature: 58,
pressure: 23,
humidity: 44,
heartRate: 77,
latitude: null,
longitude: null,
gasConcentration: 59
}
*/

#include "Arduino.h"
#include "AZ3166WiFi.h"
#include "DevKitMQTTClient.h"
#include "SystemVersion.h"
#include "Sensor.h"
#include "parson.h"

DevI2C *ext_i2c;
LSM6DSLSensor *acc_gyro;
HTS221Sensor *ht_sensor;
LIS2MDLSensor *magnetometer;
IRDASensor *IrdaSensor;
LPS22HBSensor *pressureSensor;
RGB_LED rgbLed;

static bool hasWifi = false;
static int userLEDState = 0;
static int rgbLEDState = 0;
static int rgbLEDR = 0;
static int rgbLEDG = 0;
static int rgbLEDB = 0;
// char warningMessage = "ALERT: Go to a safe area!";
static int showWarningMessage = 0;

static void InitWifi()
{
  Screen.print(2, "Connecting...");

  if (WiFi.begin() == WL_CONNECTED)
  {
    IPAddress ip = WiFi.localIP();
    Screen.print(1, ip.get_address());
    hasWifi = true;
    Screen.print(2, "Running... \r\n");
  }
  else
  {
    hasWifi = false;
    Screen.print(1, "No Wi-Fi\r\n ");
  }
}
void parseTwinMessage(DEVICE_TWIN_UPDATE_STATE updateState, const char *message)
{
    JSON_Value *root_value;
    root_value = json_parse_string(message);
    if (json_value_get_type(root_value) != JSONObject)
    {
        if (root_value != NULL)
        {
            json_value_free(root_value);
        }
        LogError("parse %s failed", message);
        return;
    }
    JSON_Object *root_object = json_value_get_object(root_value);

    if (updateState == DEVICE_TWIN_UPDATE_COMPLETE)
    {
        JSON_Object *desired_object = json_object_get_object(root_object, "desired");
        if (desired_object != NULL)
        {
          if (json_object_has_value(desired_object, "showWarningMessage"))
          {
            showWarningMessage = json_object_get_number(desired_object, "showWarningMessage");
          }
          if (json_object_has_value(desired_object, "userLEDState"))
          {
            userLEDState = json_object_get_number(desired_object, "userLEDState");
          }
          if (json_object_has_value(desired_object, "rgbLEDState"))
          {
            rgbLEDState = json_object_get_number(desired_object, "rgbLEDState");
          }
          if (json_object_has_value(desired_object, "rgbLEDR"))
          {
            rgbLEDR = json_object_get_number(desired_object, "rgbLEDR");
          }
          if (json_object_has_value(desired_object, "rgbLEDG"))
          {
            rgbLEDG = json_object_get_number(desired_object, "rgbLEDG");
          }
          if (json_object_has_value(desired_object, "rgbLEDB"))
          {
            rgbLEDB = json_object_get_number(desired_object, "rgbLEDB");
          }
        }
    }
    else
    {
      if (json_object_has_value(root_object, "showWarningMessage"))
      {
        showWarningMessage = json_object_get_number(root_object, "showWarningMessage");
      }
      if (json_object_has_value(root_object, "userLEDState"))
      {
        userLEDState = json_object_get_number(root_object, "userLEDState");
      }
      if (json_object_has_value(root_object, "rgbLEDState"))
      {
        rgbLEDState = json_object_get_number(root_object, "rgbLEDState");
      }
      if (json_object_has_value(root_object, "rgbLEDR"))
      {
        rgbLEDR = json_object_get_number(root_object, "rgbLEDR");
      }
      if (json_object_has_value(root_object, "rgbLEDG"))
      {
        rgbLEDG = json_object_get_number(root_object, "rgbLEDG");
      }
      if (json_object_has_value(root_object, "rgbLEDB"))
      {
        rgbLEDB = json_object_get_number(root_object, "rgbLEDB");
      }
    }

    if (rgbLEDState == 0)
    {
      rgbLed.turnOff();
    }
    else
    {
      rgbLed.setColor(rgbLEDR, rgbLEDG, rgbLEDB);
    }

    if(showWarningMessage == 0){
      rgbLed.setColor(0, 100, 0);
    }
    else{
      rgbLed.setColor(100, 0, 0);
      char buf[8];
      itoa(showWarningMessage, buf, 10);
      Screen.print(2, buf);

      Screen.clean();  
      Screen.print(0, "ALERT!");
      Screen.print(1, "Go to a safe    area", true);

      delay(3000);

      Screen.clean();        
      Screen.print(0, "      WSM");
      Screen.print(1, "Loading...");
    }

    pinMode(LED_USER, OUTPUT);
    digitalWrite(LED_USER, showWarningMessage);
    // digitalWrite(LED_USER, userLEDState);
    json_value_free(root_value);
}

static void DeviceTwinCallback(DEVICE_TWIN_UPDATE_STATE updateState, const unsigned char *payLoad, int size)
{
  char *temp = (char *)malloc(size + 1);
  if (temp == NULL)
  {
    return;
  }
  memcpy(temp, payLoad, size);
  temp[size] = '\0';
  parseTwinMessage(updateState, temp);
  free(temp);
}

void setup()
{
  rgbLed.turnOff();
  rgbLed.setColor(150,0,150); // LED color for Setup indication

  Screen.init();
  Screen.print(0, "      WSM");
  Screen.print(2, "Initializing...");
  Screen.print(3, " > WiFi");
  hasWifi = false;
  InitWifi();
  if (!hasWifi)
  {
    return;
  }

  Screen.print(3, " > IoT Hub");
  DevKitMQTTClient_Init(true);
  DevKitMQTTClient_SetDeviceTwinCallback(DeviceTwinCallback);

  rgbLed.setColor(0,255,0);
  Screen.clean();
}

bool i2cError = false;
int sensorMotion;
int sensorPressure;
int sensorMagnetometer;
int sensorHumidityAndTemperature;
int sensorIrda;

float temperature = 0;
float humidity = 0;
float pressure = 0;
int heartRate = 67;
float latitude = 40.767723;
float longitude = -73.976053;
float gasConcentration = 36;

char DeviceID[2] = "1";


void sendData(const char *data){
  time_t t = time(NULL);
  char buf[sizeof "2018-08-01T07:07:09Z"];
  strftime(buf, sizeof buf, "%FT%TZ", gmtime(&t));

  EVENT_INSTANCE* message = DevKitMQTTClient_Event_Generate(data, MESSAGE);

  DevKitMQTTClient_Event_AddProp(message, "$$CreationTimeUtc", buf);
  DevKitMQTTClient_Event_AddProp(message, "$$ContentType", "JSON");
  
  DevKitMQTTClient_SendEventInstance(message);
}



void loop()
{
  Screen.clean();
  Screen.print(0, "      WSM");
  Screen.print(1, "Loading...");

  DevKitMQTTClient_Check();
  const char *firmwareVersion = getDevkitVersion();
  const char *wifiSSID = WiFi.SSID();
  int wifiRSSI = WiFi.RSSI();
  const char *wifiIP = (const char *)WiFi.localIP().get_address();
  const char *wifiMask = (const char *)WiFi.subnetMask().get_address();
  byte mac[6];
  char macAddress[18];
  WiFi.macAddress(mac);
  snprintf(macAddress, 18, "%02x-%02x-%02x-%02x-%02x-%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  try
  {
    ext_i2c = new DevI2C(D14, D15);
    i2cError = false;
  }
  catch(int error)
  {
    i2cError = true;
    sensorMotion = 0;
    sensorPressure = 0;
    sensorMagnetometer = 0;
    sensorHumidityAndTemperature = 0;
    sensorIrda = 0;
  }

  int sensorInitResult;

  if (!i2cError)
  {
    // try
    // {
    //   acc_gyro = new LSM6DSLSensor(*ext_i2c, D4, D5);
    //   sensorInitResult = acc_gyro->init(NULL);
    //   acc_gyro->enableAccelerator();
    //   acc_gyro->enableGyroscope();

    //   if (sensorInitResult == 0)
    //   {
    //     sensorMotion = 1;
    //   }
    //   else
    //   {
    //     sensorMotion = 0;
    //   }
    // }
    // catch(int error)
    // {
    //   sensorMotion = 0;
    // }

    try
    {
      ht_sensor = new HTS221Sensor(*ext_i2c);
      sensorInitResult = ht_sensor->init(NULL);

      if (sensorInitResult == 0)
      {
        sensorHumidityAndTemperature = 1;
        
        ht_sensor->reset();
        ht_sensor->getHumidity(&humidity);

        // Screen.print(0, "WSM");

        ht_sensor->getTemperature(&temperature);

        char buff[128];
        sprintf(buff, "Temp:%s%c    \r\nHumidity:%s%c",f2s(temperature, 1),'F', f2s(humidity, 1), '%');
        Screen.print(1, buff);

        delay(5000);
      }
      else
      {
        sensorHumidityAndTemperature = 0;
      }
    }
    catch(int error)
    {
      sensorHumidityAndTemperature = 0;
    }

    // try
    // {
    //   magnetometer = new LIS2MDLSensor(*ext_i2c);
    //   sensorInitResult = magnetometer->init(NULL);

    //   if (sensorInitResult == 0)
    //   {
    //     sensorMagnetometer = 1;
    //   }
    //   else
    //   {
    //     sensorMagnetometer = 0;
    //   }
    // }
    // catch(int error)
    // {
    //   sensorMagnetometer = 0;
    // }

    try
    {
      pressureSensor = new LPS22HBSensor(*ext_i2c);
      sensorInitResult = pressureSensor -> init(NULL);

      if (sensorInitResult == 0)
      {
        sensorPressure = 1;

        pressureSensor->getPressure(&pressure);

        char buff[128];
        sprintf(buff, "%s%s",f2s(pressure, 1),"mbar");
        Screen.print(1,"Pressure:");
        Screen.print(2, buff);

      }
      else
      {
        sensorPressure = 0;
      }
    }
    catch(int error)
    {
      sensorPressure = 0;
    }

    // try
    // {
    //   IrdaSensor = new IRDASensor();
    //   sensorInitResult = IrdaSensor->init();

    //   if (sensorInitResult == 0)
    //   {
    //     sensorIrda = 1;
    //   }
    //   else
    //   {
    //     sensorIrda = 0;
    //   }
    // }
    // catch(int error)
    // {
    //   sensorIrda = 0;
    // }
  }

  // if (rgbLEDState == 0)
  // {
  //   rgbLed.turnOff();
  // }
  // else
  // {
  //   rgbLed.setColor(rgbLEDR, rgbLEDG, rgbLEDB);
  // }


  // Generate Random values
  heartRate = random(60, 80);
  gasConcentration = random(0, 3);
  latitude = random(40764300, 40764306) / 1000000.00;
  longitude = random(-73973018, -73972996) / 1000000.00;

  char sensorData[200];

  char heartRateBuf[8];      
  //{ deviceID: "devkit", timestamp: null, temperature: 58, pressure: 23, humidity: 44, heartRate: 77, latitude: null, longitude: null, gasConcentration: 59 } 
  // sprintf_s(sensorData, sizeof(sensorData), "{\"temperature\":%s,\"temperature_unit\":\"%c\",\"humidity\":%s,\"humidity_unit\":\"%c\"}", f2s(temperature, 1), 'F',f2s(humidity, 1), '%');
  sprintf_s(sensorData, sizeof(sensorData), "{\"deviceID\":'%s',\"temperature\":null,\"temperature\":%s,\"pressure\":%s,\"humidity\":%s,\"heartRate\":%s,\"latitude\":%s,\"longitude\":%s,\"gasConcentration\":%s}", DeviceID, f2s(temperature, 1), f2s(pressure, 1), f2s(humidity, 1), itoa(heartRate, heartRateBuf, 10), f2s(latitude, 6), f2s(longitude, 6), f2s(gasConcentration, 1));
  sendData(sensorData);   


  pinMode(LED_USER, OUTPUT);
  digitalWrite(LED_USER, userLEDState);

  char state[500];
  snprintf(state, 500, "{\"firmwareVersion\":\"%s\",\"wifiSSID\":\"%s\",\"wifiRSSI\":%d,\"wifiIP\":\"%s\",\"wifiMask\":\"%s\",\"macAddress\":\"%s\",\"sensorMotion\":%d,\"sensorPressure\":%d,\"sensorMagnetometer\":%d,\"sensorHumidityAndTemperature\":%d,\"sensorIrda\":%d}", firmwareVersion, wifiSSID, wifiRSSI, wifiIP, wifiMask, macAddress, sensorMotion, sensorPressure, sensorMagnetometer, sensorHumidityAndTemperature, sensorIrda);
  DevKitMQTTClient_ReportState(state);
  delay(5000);
}
