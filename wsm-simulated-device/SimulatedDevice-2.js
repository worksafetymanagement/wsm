// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

'use strict';

// The device connection string to authenticate the device with your IoT hub.
//
// NOTE:
// For simplicity, this sample sets the connection string in code.
// In a production environment, the recommended approach is to use
// an environment variable to make it available to your application
// or use an HSM or an x509 certificate.
// https://docs.microsoft.com/azure/iot-hub/iot-hub-devguide-security
//
// Using the Azure CLI:
// az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
var connectionString = 'HostName=wsm.azure-devices.net;DeviceId=device2;SharedAccessKey=FeIs53hgJpTYPs858/dSXZG7QFFUCnA5UXchWRMeDsY=';
var deviceID = 2;

// Using the Node.js Device SDK for IoT Hub:
//   https://github.com/Azure/azure-iot-sdk-node
// The sample connects to a device-specific MQTT endpoint on your IoT Hub.
var Mqtt = require('azure-iot-device-mqtt').Mqtt;
var DeviceClient = require('azure-iot-device').Client
var Message = require('azure-iot-device').Message;

var client = DeviceClient.fromConnectionString(connectionString, Mqtt);

// Print results.
function printResultFor(op) {
  return function printResult(err, res) {
    if (err) console.log(op + ' error: ' + err.toString());
    if (res) console.log(op + ' status: ' + res.constructor.name);
  };
}

// Create a message and send it to the IoT hub every second
setInterval(function () {
  // Simulate telemetry.
  var temperature = (70 + (Math.random() * 21)).toFixed(2);
  var humidity = (50 + (Math.random() * 36)).toFixed(2);
  var pressure = (900 + (Math.random() * 210)).toFixed(2);

  var heartRate = Math.floor(60 + (Math.random() * 30));
  var gasConcentration = (0 + (Math.random() * 3)).toFixed(2);

  var latitude = ((40764300 + (Math.random() * 3000)) / 1000000.00).toFixed(6);
  var longitude = ((-73973018 + (Math.random() * 2000)) / 1000000.00).toFixed(6);

  // var latitude = random(40764300, 40764306) / 1000000.00;
  // var longitude = random(-73973018, -73972996) / 1000000.00;


  // Add the telemetry to the message body.
  // var data = JSON.stringify({ temperature: temperature, humidity: humidity });

  var data = JSON.stringify({ deviceID: deviceID, temperature: temperature, pressure: pressure, humidity: humidity, heartRate: heartRate, latitude: latitude, longitude: longitude, gasConcentration: gasConcentration });

  // sprintf_s(sensorData, sizeof(sensorData), "{\"deviceID\":'%s',\"temperature\":null,\"temperature\":%s,\"pressure\":%s,\"humidity\":%s,\"heartRate\":%s,\"latitude\":%s,\"longitude\":%s,\"gasConcentration\":%s}", DeviceID, f2s(temperature, 1), f2s(pressure, 1), f2s(humidity, 1), itoa(heartRate, heartRateBuf, 10), f2s(latitude, 6), f2s(longitude, 6), f2s(gasConcentration, 1));


  var message = new Message(data);

  // // Add a custom application property to the message.
  // // An IoT hub can filter on these properties without access to the message body.
  // message.properties.add('temperatureAlert', (temperature > 30) ? 'true' : 'false');
  
  console.log('Sending message: ' + message.getData());

  // Send the message.
  client.sendEvent(message, printResultFor('send'));
}, 10000);
