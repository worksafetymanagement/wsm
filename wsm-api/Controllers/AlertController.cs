using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using WSM.WebApi.Models;

namespace WSM.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="WSM.WebApi.Controllers.BaseApiController" />
    [Route("api/alerts")]
    public class AlertController : BaseApiController
    {
        public AlertController(WsmDbContext context) :base(context)
        {

        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<Alert>> GetAll()
        {
            return this.DataContext.Alerts.Where(a => a.AlertTime > DateTime.Now.AddHours(-24)).ToList();
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public ActionResult<Alert> GetById(int id)
        {
            var alert = this.DataContext.Alerts.FirstOrDefault(a => a.AlertId == id);
            if (alert == null)
            {
                return NotFound();
            }

            return alert;
        }
    }
}