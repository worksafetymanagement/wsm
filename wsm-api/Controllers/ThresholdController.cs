using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using WSM.WebApi.Models;

namespace WSM.WebApi.Controllers
{
    [Route("api/thresholds")]
    public class ThresholdController : BaseApiController
    {
        public ThresholdController(WsmDbContext context) :base(context)
        {

        }

        [HttpGet]
        public ActionResult<List<Threshold>> GetAll()
        {
            return this.DataContext.Thresholds.ToList();
        }

        [Route("alert")]
        [HttpPost]
        public ActionResult<bool> HasAlertStatus(WorkerStatus currentStatus)
        {
            var thresholds = this.DataContext.Thresholds?.ToList();
            if (thresholds == null || currentStatus == null)
            {
                return false;
            }

            if (thresholds.Any(t => 
                                t.Type == ThresholdType.HeartRate 
                                && t.CompareOperator == ThresholdCompareOperator.GreaterThan
                                && currentStatus.HeartRate > t.Value))
            {
                return true;
            };

            if (thresholds.Any(t =>
                                t.Type == ThresholdType.Temperature
                                && t.CompareOperator == ThresholdCompareOperator.GreaterThan
                                && currentStatus.Temperature > t.Value))
            {
                return true;
            };

            if (thresholds.Any(t =>
                                t.Type == ThresholdType.Pressure
                                && t.CompareOperator == ThresholdCompareOperator.GreaterThan
                                && currentStatus.Pressure > t.Value))
            {
                return true;
            };

            if (thresholds.Any(t =>
                                t.Type == ThresholdType.Humidity
                                && t.CompareOperator == ThresholdCompareOperator.GreaterThan
                                && currentStatus.Humidity > t.Value))
            {
                return true;
            };

            if (thresholds.Any(t =>
                                t.Type == ThresholdType.GasConcentration
                                && t.CompareOperator == ThresholdCompareOperator.GreaterThan
                                && currentStatus.GasConcentration > t.Value))
            {
                return true;
            };

            return false;
        }
    }
}