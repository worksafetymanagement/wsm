using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using WSM.WebApi.Models;

namespace WSM.WebApi.Controllers
{
    [ApiController]
    public class BaseApiController : ControllerBase
    {
        protected WsmDbContext DataContext {get; private set;}
        
        public BaseApiController(WsmDbContext context) 
        {
            DataContext = context;
        }
    }
}