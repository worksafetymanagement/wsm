using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using WSM.WebApi.Models;

namespace WSM.WebApi.Controllers
{
    [Route("api/devices")]
    public class DeviceController : BaseApiController
    {
        public DeviceController(WsmDbContext context) :base(context)
        {

        }

        [HttpGet]
        public ActionResult<List<Device>> GetAll()
        {
            return this.DataContext.Devices.ToList();
        }
    }
}