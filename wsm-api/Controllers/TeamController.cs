using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using WSM.WebApi.Models;

namespace WSM.WebApi.Controllers
{
    [Route("api/teams")]
    public class TeamController : BaseApiController
    {
        public TeamController(WsmDbContext context) :base(context)
        {

        }
        
        [HttpGet]
        public ActionResult<List<Team>> GetAll()
        {
            var teams = this.DataContext.Teams
                .Include(t => t.Members).ToList();

            //teams?.ForEach(t =>
            //{
            //    foreach(var member in t.Members)
            //    {
            //        member.LastWorkerLocation =
            //        this.DataContext.WorkerStatuses.OrderByDescending(s => s.Timestamp).Take(1).Select(st => new WorkerLocation { DeviceId = st.DeviceId, Latitude = st.Latitude, Longitude = st.Longitude, Time = st.Timestamp, WorkerId = st.WorkerId })?.FirstOrDefault();
            //    }
            //});

            return teams;
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult<Team> GetById(int id)
        {
            var team = this.DataContext.Teams.Include(t => t.Members).FirstOrDefault(t => t.TeamId == id);
            if (team == null)
            {
                return NotFound();
            }

            return team;
        }
    }
}