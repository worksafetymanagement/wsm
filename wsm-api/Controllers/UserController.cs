using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using WSM.WebApi.Models;

namespace WSM.WebApi.Controllers
{
    [Route("api/users")]
    public class UserController : BaseApiController
    {
        public UserController(WsmDbContext context) :base(context)
        {

        }

        [HttpGet]
        public ActionResult<List<User>> GetAll()
        {
            return this.DataContext.Users.Include(u => u.Team).ToList();
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult<User> GetById(int id)
        {
            var user = this.DataContext.Users.Include(u => u.Team).FirstOrDefault(t => t.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [Route("{id}/status")]
        [HttpPost]
        public ActionResult<User> CheckStatus(int id, WorkerStatus currentStatus)
        {
            var user = this.DataContext.Users.FirstOrDefault(t => t.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return user;
        }
    }
}