﻿
IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Teams] (
    [TeamId] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [SupervisorId] int NOT NULL,
    CONSTRAINT [PK_Teams] PRIMARY KEY ([TeamId])
);

GO

CREATE TABLE [Thresholds] (
    [ThresholdId] int NOT NULL IDENTITY,
    [Type] int NOT NULL,
    [Value] float NOT NULL,
    [AlertLevel] int NOT NULL,
    [CompareOperator] int NOT NULL,
    CONSTRAINT [PK_Thresholds] PRIMARY KEY ([ThresholdId])
);

GO

CREATE TABLE [Users] (
    [UserId] int NOT NULL IDENTITY,
    [FirstName] nvarchar(max) NULL,
    [LastName] nvarchar(max) NULL,
    [Phone] nvarchar(max) NULL,
    [TeamId] int NULL,
    [TempId] int NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([UserId]),
    CONSTRAINT [AK_Users_TempId] UNIQUE ([TempId]),
    CONSTRAINT [FK_Users_Teams_TeamId] FOREIGN KEY ([TeamId]) REFERENCES [Teams] ([TeamId]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Devices] (
    [Id] int NOT NULL IDENTITY,
    [DeviceId] nvarchar(max) NOT NULL,
    [Name] nvarchar(max) NULL,
    [CurrentUserId] int NOT NULL,
    [UserId] int NULL,
    [Type] nvarchar(max) NULL,
    CONSTRAINT [PK_Devices] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Devices_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([UserId]) ON DELETE NO ACTION
);

GO

CREATE TABLE [WorkerStatus] (
    [WorkerStatusId] int NOT NULL IDENTITY,
    [DeviceId] nvarchar(max) NOT NULL,
    [Timestamp] datetime2 NOT NULL,
    [Temperature] float NOT NULL,
    [Pressure] float NOT NULL,
    [Humidity] float NOT NULL,
    [HeartRate] int NOT NULL,
    [Latitude] float NOT NULL,
    [Longitude] float NOT NULL,
    [GasConcentration] float NOT NULL,
    CONSTRAINT [PK_WorkerStatuses] PRIMARY KEY ([WorkerStatusId]),
);

GO

CREATE INDEX [IX_Devices_UserId] ON [Devices] ([UserId]);

GO

CREATE INDEX [IX_Users_TeamId] ON [Users] ([TeamId]);

GO

CREATE INDEX [IX_WorkerStatuses_DeviceId] ON [WorkerStatuses] ([DeviceId]);

GO

CREATE INDEX [IX_WorkerStatuses_WorkerId] ON [WorkerStatuses] ([WorkerId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180802030824_InitialCreate', N'2.1.1-rtm-30846');

GO

