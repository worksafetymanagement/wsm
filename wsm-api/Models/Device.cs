namespace WSM.WebApi.Models
{
    public class Device
    {
        public int DeviceId { get; set; }

        public string Name { get; set; }

        public int CurrentUserId { get; set; }

        public User User { get; set; }

        public string Type { get; set; }
    }
}