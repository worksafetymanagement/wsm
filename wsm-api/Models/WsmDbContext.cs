using Microsoft.EntityFrameworkCore;

namespace WSM.WebApi.Models
{
    public class WsmDbContext : DbContext
    {
        public WsmDbContext(DbContextOptions<WsmDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Query<Alert>().ToView("vwAlerts");
        }

        public DbSet<User> Users {get; set;}

        public DbQuery<Alert> Alerts {get; set;}

        public DbSet<Device> Devices {get; set;}

        public DbSet<Threshold> Thresholds {get; set;}

        public DbSet<Team> Teams {get; set;}

        public DbSet<WorkerStatus> WorkerStatus {get; set;}
    }
}