namespace WSM.WebApi.Models
{
    public enum ThresholdType : int 
    {
        Temperature = 1,
        Humidity,
        Pressure,
        HeartRate,
        GasConcentration        
    }

    public enum ThresholdCompareOperator : int 
    {
        Equals = 1,
        GreaterThan,
        LessThan
    }

    public class Threshold
    {
       public int ThresholdId { get; set; }

       public ThresholdType Type { get; set; }

       public double Value { get; set; }

       public AlertLevel AlertLevel { get; set; }

        public ThresholdCompareOperator CompareOperator { get; set; }
    }
}