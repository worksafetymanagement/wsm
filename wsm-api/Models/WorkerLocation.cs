using System;

namespace WSM.WebApi.Models
{
    public class WorkerLocation
    {
        public int WorkerId { get; set;}

        public DateTime Time { get; set; }

        public AlertLevel AlertLevel { get; set; }

        public int DeviceId { get; set; }

        public Device Device { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}