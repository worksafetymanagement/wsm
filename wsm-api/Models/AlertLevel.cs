namespace WSM.WebApi.Models
{
    public enum AlertLevel : int 
    {
        Low = 1,
        Medium,
        High,
        None
    }
}