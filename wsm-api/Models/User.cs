using System.ComponentModel.DataAnnotations.Schema;

namespace WSM.WebApi.Models
{
    public class User
    {
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        [NotMapped]
        public string DeviceStatus { get; set; }

        [NotMapped]
        public WorkerLocation LastWorkerLocation { get; set; }

        public Team Team { get; set; }
    }
}