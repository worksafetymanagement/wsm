using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WSM.WebApi.Models
{
    public class Alert
    {
        public int AlertId { get; set; }

        public string Description { get; set; } //Alert Level

        public int WorkerId { get; set;}

        [NotMapped]
        public User Worker { get; set; }

        public AlertLevel AlertLevel { get; set; }

        public DateTime AlertTime {get; set;}

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Reading { get; set; }
    }
}