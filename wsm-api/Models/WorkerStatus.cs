using System;
using System.Collections.Generic;

namespace WSM.WebApi.Models
{
    public class WorkerStatus
    {
        public int WorkerStatusId { get; set; }

        public int DeviceId { get; set; }

        public Device Device { get; set; }

        // public int WorkerId { get; set; }

        // public User Worker { get; set; }

        public DateTime Timestamp { get; set; }

        public double Temperature { get; set; }

        public double Pressure { get; set; }

        public double Humidity { get; set; }

        public int HeartRate { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double GasConcentration { get; set; }

        //public ICollection<Threshold> CurrentThresholds { get; set; }
    }
}