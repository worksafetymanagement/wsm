using System.Collections.Generic;

namespace WSM.WebApi.Models
{
    public class Team
    {
        public int TeamId { get; set; }

        public string Name { get; set; }

        public int SupervisorId { get; set; }
        
        public ICollection<User> Members { get; set; }
    }
}