using System.Net;
using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Threading.Tasks;

public static async Task Run(string myIoTHubMessage, TraceWriter log)
{
    log.Info($"C# IoT Hub trigger function processed a message: {myIoTHubMessage}");
    
    //return;
    
    var e = JsonConvert.DeserializeObject<WorkerStatus>(myIoTHubMessage);
    e.timestamp = System.DateTime.Now;

    // HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Saved");
    
    try
    {        
        using (WorkerStatusContext context = new WorkerStatusContext())
        {
            // context.WorkerStatus.ToList().ForEach(u => 
            // log.Info($"WokerStatus: {u.deviceID}"));

            context.WorkerStatus.Add(e); 
            await context.SaveChangesAsync();
        }

        return;
    }
    catch(System.Data.Entity.Infrastructure.DbUpdateException ex)
    {
        log.Info(string.Format("Failure with database update {0}.", ex.Message));   
        return;
    }  

}

public class WorkerStatusContext : DbContext
{    
    public WorkerStatusContext()
        : base("Server=tcp:worksafetymanagement.database.windows.net,1433;Initial Catalog=wsm;Persist Security Info=False;User ID=wsm;Password=@m@nM1kH@1L;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;")
    { }

    public DbSet<WorkerStatus> WorkerStatus { get; set; }
}

public class WorkerStatus
{
    public int ID { get; set; }
    public string deviceID { get; set; }
    public DateTime? timestamp { get; set; }
    public double? temperature { get; set; }
    public double? pressure { get; set; }
    public double? humidity { get; set; }
    public int? heartRate { get; set; }
    public double? latitude { get; set; }
    public double? longitude { get; set; }
    public double? gasConcentration { get; set; }
}