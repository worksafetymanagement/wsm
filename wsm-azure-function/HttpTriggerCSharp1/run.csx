using System.Net;
using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Threading.Tasks;

public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, TraceWriter log)
{
    // log.Info($"C# IoT Hub trigger function processed a message: {myIoTHubMessage}");

    dynamic body = await req.Content.ReadAsStringAsync();
    var e = JsonConvert.DeserializeObject<WorkerStatus>(body as string);
    e.timestamp = System.DateTime.Now;

    // log.Info($"e= {e.ToString()}");    

    // WorkerStatus testVal = new WorkerStatus(){
    //     deviceID = "Device1",
    //     timestamp = DateTime.Now,
    //     temperature = 88,
    //     pressure = 23,
    //     humidity = 44,
    //     heartRate = 77,
    //     latitude = null,
    //     longitude = null,
    //     gasConcentration = 59
    // };

    try
    {        
        using (WorkerStatusContext context = new WorkerStatusContext())
        {
            // context.WorkerStatus.ToList().ForEach(u => 
            // log.Info($"WokerStatus: {u.deviceID}"));

            context.WorkerStatus.Add(e); 
            await context.SaveChangesAsync();
        }

        return req.CreateResponse(HttpStatusCode.Created, e);    
    }
    catch(System.Data.Entity.Infrastructure.DbUpdateException ex)
    {
        log.Info(string.Format("Failure with database update {0}.", ex.Message));   
        return req.CreateResponse(HttpStatusCode.NotFound, e);          
    }  
}


public class WorkerStatusContext : DbContext
{
    // string _dbConnString = "Server=tcp:worksafetymanagement.database.windows.net,1433;Initial Catalog=wsm;Persist Security Info=False;User ID=wsm;Password=@m@nM1kH@1L;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
    
    public WorkerStatusContext()
        : base("Server=tcp:worksafetymanagement.database.windows.net,1433;Initial Catalog=wsm;Persist Security Info=False;User ID=wsm;Password=@m@nM1kH@1L;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;")
    { }

    public DbSet<WorkerStatus> WorkerStatus { get; set; }
}

public class WorkerStatus
{
    public int ID { get; set; }
    public string deviceID { get; set; }
    public DateTime? timestamp { get; set; }
    public double? temperature { get; set; }
    public double? pressure { get; set; }
    public double? humidity { get; set; }
    public int? heartRate { get; set; }
    public double? latitude { get; set; }
    public double? longitude { get; set; }
    public double? gasConcentration { get; set; }
}